﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI
{
    public partial class GUIBuscarProducto : Form
    {
        public GUIBuscarProducto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ServicioProductoSW.ProductSWClient servicioProduct;
            servicioProduct = new ServicioProductoSW.ProductSWClient();

            int code = Convert.ToInt32(txtCodigoBuscado.Text);

            try
            {
                ServicioProductoSW.productDTO product = servicioProduct.findProduct(code);
                txtCodigo.Text = Convert.ToString(product.code);
                txtNombre.Text = Convert.ToString(product.name);
                txtPrecio.Text = Convert.ToString(product.price);

                String titulo = " INFORMATION ";
                MessageBox.Show("Buscado  Correctamente ", titulo);
                
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
