﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI
{
    public partial class GUIListarProductos : Form
    {
        public GUIListarProductos()
        {
            InitializeComponent();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();

            ServicioProductoSW.ProductSWClient servicioProduct;
            servicioProduct = new ServicioProductoSW.ProductSWClient();

            ServicioProductoSW.productDTO [] productos;

            DataTable dt = new DataTable();
            DataRow dr;

            dt.Columns.Add(new DataColumn("Código"));
            dt.Columns.Add(new DataColumn("Nombre"));
            dt.Columns.Add(new DataColumn("Precio"));


            try
            {
                productos = servicioProduct.getAllProducts();

                for (int i = 0; i < productos.Length; i++)
                {
                    dr = dt.NewRow();
                    dr["Código"] = Convert.ToString(productos[i].code);
                    dr["Nombre"] = productos[i].name;
                    dr["Precio"] = Convert.ToString(productos[i].price);

                    dt.Rows.Add(dr);

                }
                dataGridView1.DataSource = dt;
                    
                String titulo = " INFORMATION ";
                MessageBox.Show("Listado Correctamente ", titulo);


            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
