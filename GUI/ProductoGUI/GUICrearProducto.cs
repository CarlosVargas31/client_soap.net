﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI
{
    public partial class GUICrearProducto : Form
    {
        public GUICrearProducto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            ServicioProductoSW.ProductSWClient servicioProduct;
            servicioProduct = new ServicioProductoSW.ProductSWClient();

            ServicioProductoSW.productDTO product = new ServicioProductoSW.productDTO();

            int codigo =Convert.ToInt32( txtCodigo.Text);
            String nombre = txtNombre.Text;
            double precio = Convert.ToDouble(txtPrecio.Text);

            product.code = codigo;
            product.name = nombre;
            product.price = precio;

            try
            {
                servicioProduct.saveProduct(product);
                String titulo = " INFORMATION ";
                MessageBox.Show("Adicionado Correctamente " , titulo);

                txtCodigo.Text = "";
                txtNombre.Text = "";
                txtPrecio.Text = "";
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error "+ ex, titulo);

            }



        }
    }
}
