﻿using ProyectoClienteSw.GUI;
using ProyectoClienteSw.GUI.LoteGUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw
{
    public partial class GUIPrincipal : Form
    {
        public GUIPrincipal()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void crearProductoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUICrearProducto gui = new GUICrearProducto();
            gui.Show();
        }

        private void eliminarProductoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIEliminar_producto gg = new GUIEliminar_producto();
            gg.Show();
        }

        private void buscarProductoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIBuscarProducto ff = new GUIBuscarProducto();
            ff.Show();
        }

        private void listarProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIListarProductos nn = new GUIListarProductos();
            nn.Show();
        }

        private void actualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIActualizarProducto ss = new GUIActualizarProducto();
            ss.Show();
        }

        private void buscarPorParametroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIBuscarporParametro ss = new GUIBuscarporParametro();
            ss.Show();
        }

        private void listarPorParametroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIListarParametro ss = new GUIListarParametro();
            ss.Show();
        }

        private void adicionarLoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUICrearLote ss = new GUICrearLote();
            ss.Show();
        }

        private void cToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIEliminarLote ss = new GUIEliminarLote();
            ss.Show();
        }

        private void buscarLoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIBuscarLote ss = new GUIBuscarLote();
            ss.Show();
        }

        private void listarLoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIListarLotes ss = new GUIListarLotes();
            ss.Show();
        }

        private void actualizarLoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIActualizarLote ss = new GUIActualizarLote();
            ss.Show();
        }

        private void buscarPorParametroLoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIBuscarPorParametroLote ss = new GUIBuscarPorParametroLote();
            ss.Show();
        }

        private void listarPorParametroLoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIListarPorParametroLote ss = new GUIListarPorParametroLote();
            ss.Show();
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String titulo = " INFORMATION ";
            MessageBox.Show("Este programa fue desarrollado por: Norberto Guerrero Moya & Carlos Andres Vargas Bermudez ", titulo);

        }

        private void informaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
