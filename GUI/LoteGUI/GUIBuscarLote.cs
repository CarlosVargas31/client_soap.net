﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI.LoteGUI
{
    public partial class GUIBuscarLote : Form
    {
        public GUIBuscarLote()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ServicioLoteSW.LotSWClient servicioLotes;
            servicioLotes = new ServicioLoteSW.LotSWClient();

            ServicioLoteSW.lotDTO lotes = new ServicioLoteSW.lotDTO();

            int code = Convert.ToInt32(txtCodigoBuscado.Text);

            try
            {
                lotes = servicioLotes.findLot(code);

                txtIdInterno.Text = Convert.ToString(lotes.id);
                txtAlmacen.Text = Convert.ToString(lotes.warehouse.name);
                txtProducto.Text = Convert.ToString(lotes.product.name);
                txtStock.Text = Convert.ToString(lotes.stock);

                String titulo = " INFORMATION ";
                MessageBox.Show("Buscado  Correctamente ", titulo);
             
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
