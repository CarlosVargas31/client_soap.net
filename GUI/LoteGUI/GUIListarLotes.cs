﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI.LoteGUI
{
    public partial class GUIListarLotes : Form
    {
        public GUIListarLotes()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
             dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();

            ServicioLoteSW.LotSWClient servicioLotes;
            servicioLotes = new ServicioLoteSW.LotSWClient();

            ServicioLoteSW.lotDTO [] lotes;

            DataTable dt = new DataTable();
            DataRow dr;

            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Almacen"));
            dt.Columns.Add(new DataColumn("Producto"));
            dt.Columns.Add(new DataColumn("Cantidad"));


            try
            {
                lotes = servicioLotes.getAllLots();

                for (int i = 0; i < lotes.Length; i++)
                {
                    dr = dt.NewRow();
                    dr["Id"] = Convert.ToString(lotes[i].id);
                    dr["Almacen"] = Convert.ToString(lotes[i].warehouse.name);
                    dr["Producto"] = Convert.ToString(lotes[i].product.name);
                    dr["Cantidad"] = Convert.ToString(lotes[i].stock);

                    dt.Rows.Add(dr);

                }
                dataGridView1.DataSource = dt;
                    
                String titulo = " INFORMATION ";
                MessageBox.Show("Listado Correctamente ", titulo);


            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
