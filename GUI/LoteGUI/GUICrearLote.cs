﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI.LoteGUI
{
    public partial class GUICrearLote : Form
    {
        public GUICrearLote()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ServicioLoteSW.LotSWClient servicioLotes;
            servicioLotes = new ServicioLoteSW.LotSWClient();

            ServicioWarehouseSW.WarehouseSWClient servicioWarehouse;
            servicioWarehouse = new ServicioWarehouseSW.WarehouseSWClient();

            ServicioProductoSW.ProductSWClient servicioProduct;
            servicioProduct = new ServicioProductoSW.ProductSWClient();

            ServicioLoteSW.lotDTO lotes = new ServicioLoteSW.lotDTO();
            ServicioProductoSW.productDTO products = new ServicioProductoSW.productDTO(); ;
            ServicioWarehouseSW.warehouseDTO warehouses = new ServicioWarehouseSW.warehouseDTO();

            ServicioLoteSW.warehouseDTO warehouse2 = new ServicioLoteSW.warehouseDTO();
            ServicioLoteSW.productDTO product2 = new ServicioLoteSW.productDTO();




            long codigo = Convert.ToInt64(txtIdInterno.Text);
            int codeWarehouse = Convert.ToInt32(txtAlmacen.Text);
            int codeProduct = Convert.ToInt32(txtProducto.Text);
            int cantidad = Convert.ToInt32(txtStock.Text);




            try
            {
                warehouses = servicioWarehouse.findWarehouses(codeWarehouse);
                products = servicioProduct.findProduct(codeProduct);

               


                lotes.id = codigo;

                warehouse2.id = warehouses.id;
                warehouses.name = warehouses.name;
                warehouse2.address = warehouses.address;
                lotes.warehouse = warehouse2;


                product2.code = products.code;
                product2.name = products.name;
                product2.price = products.price;
                lotes.product = product2;

                

                lotes.stock = Convert.ToInt32(txtStock.Text);
                lotes.idSpecified = true;
                lotes.stockSpecified = true;
                
                servicioLotes.saveLot(lotes,codeProduct,codeWarehouse);

                
                String titulo = " INFORMATION ";
                MessageBox.Show("Adicionado Correctamente ", titulo);

                txtAlmacen.Text = "";
                txtIdInterno.Text = "";
                txtProducto.Text = "";
                txtIdInterno.Text = "";
                txtStock.Text = "";

            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
