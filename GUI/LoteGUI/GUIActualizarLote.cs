﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI.LoteGUI
{
    public partial class GUIActualizarLote : Form
    {
        public GUIActualizarLote()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ServicioLoteSW.LotSWClient servicioLotes;
            servicioLotes = new ServicioLoteSW.LotSWClient();

            ServicioLoteSW.lotDTO lotes = new ServicioLoteSW.lotDTO();

            int code = Convert.ToInt32(txtCodigoBuscado.Text);

            try
            {
                lotes = servicioLotes.findLot(code);

                txtIdInterno.Text = Convert.ToString(lotes.id);
                txtAlmacen.Text = Convert.ToString(lotes.warehouse.name);
                txtProducto.Text = Convert.ToString(lotes.product.name);
                txtStock.Text = Convert.ToString(lotes.stock);

                String titulo = " INFORMATION ";
                MessageBox.Show("Buscado  Correctamente ", titulo);
                button2.Enabled = true;
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ServicioLoteSW.LotSWClient servicioLotes;
            servicioLotes = new ServicioLoteSW.LotSWClient();

            long code = Convert.ToInt64(txtCodigoBuscado.Text);
            ServicioLoteSW.lotDTO lotes = new ServicioLoteSW.lotDTO();
            try
            {
                lotes = servicioLotes.findLot(code);


                lotes.stock = Convert.ToInt32(txtStock.Text);

                servicioLotes.updateLot(code, lotes);

               
                String titulo = " INFORMATION ";
                MessageBox.Show("Actualizado  Correctamente ", titulo);

                txtIdInterno.Text = "";
                txtProducto.Text = "";
                txtAlmacen.Text = "";
                txtStock.Text = "";
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
