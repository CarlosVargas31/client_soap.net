﻿namespace ProyectoClienteSw
{
    partial class GUIPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearProductoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarProductoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarProductoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarProductosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarPorParametroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarPorParametroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lotesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adicionarLoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarLoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarLoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarLoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarPorParametroLoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarPorParametroLoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Desktop;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.productToolStripMenuItem,
            this.lotesToolStripMenuItem,
            this.informaciónToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(73, 24);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(121, 26);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // productToolStripMenuItem
            // 
            this.productToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearProductoToolStripMenuItem,
            this.eliminarProductoToolStripMenuItem,
            this.buscarProductoToolStripMenuItem,
            this.listarProductosToolStripMenuItem,
            this.actualizarToolStripMenuItem,
            this.buscarPorParametroToolStripMenuItem,
            this.listarPorParametroToolStripMenuItem});
            this.productToolStripMenuItem.Name = "productToolStripMenuItem";
            this.productToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.productToolStripMenuItem.Text = "Producto";
            // 
            // crearProductoToolStripMenuItem
            // 
            this.crearProductoToolStripMenuItem.Name = "crearProductoToolStripMenuItem";
            this.crearProductoToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.crearProductoToolStripMenuItem.Text = "Crear Producto";
            this.crearProductoToolStripMenuItem.Click += new System.EventHandler(this.crearProductoToolStripMenuItem_Click);
            // 
            // eliminarProductoToolStripMenuItem
            // 
            this.eliminarProductoToolStripMenuItem.Name = "eliminarProductoToolStripMenuItem";
            this.eliminarProductoToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.eliminarProductoToolStripMenuItem.Text = "Eliminar Producto";
            this.eliminarProductoToolStripMenuItem.Click += new System.EventHandler(this.eliminarProductoToolStripMenuItem_Click);
            // 
            // buscarProductoToolStripMenuItem
            // 
            this.buscarProductoToolStripMenuItem.Name = "buscarProductoToolStripMenuItem";
            this.buscarProductoToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.buscarProductoToolStripMenuItem.Text = "Buscar Producto";
            this.buscarProductoToolStripMenuItem.Click += new System.EventHandler(this.buscarProductoToolStripMenuItem_Click);
            // 
            // listarProductosToolStripMenuItem
            // 
            this.listarProductosToolStripMenuItem.Name = "listarProductosToolStripMenuItem";
            this.listarProductosToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.listarProductosToolStripMenuItem.Text = "Listar Productos";
            this.listarProductosToolStripMenuItem.Click += new System.EventHandler(this.listarProductosToolStripMenuItem_Click);
            // 
            // actualizarToolStripMenuItem
            // 
            this.actualizarToolStripMenuItem.Name = "actualizarToolStripMenuItem";
            this.actualizarToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.actualizarToolStripMenuItem.Text = "Actualizar Producto";
            this.actualizarToolStripMenuItem.Click += new System.EventHandler(this.actualizarToolStripMenuItem_Click);
            // 
            // buscarPorParametroToolStripMenuItem
            // 
            this.buscarPorParametroToolStripMenuItem.Name = "buscarPorParametroToolStripMenuItem";
            this.buscarPorParametroToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.buscarPorParametroToolStripMenuItem.Text = "Buscar Por Parametro";
            this.buscarPorParametroToolStripMenuItem.Click += new System.EventHandler(this.buscarPorParametroToolStripMenuItem_Click);
            // 
            // listarPorParametroToolStripMenuItem
            // 
            this.listarPorParametroToolStripMenuItem.Name = "listarPorParametroToolStripMenuItem";
            this.listarPorParametroToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.listarPorParametroToolStripMenuItem.Text = "Listar Por Parametro";
            this.listarPorParametroToolStripMenuItem.Click += new System.EventHandler(this.listarPorParametroToolStripMenuItem_Click);
            // 
            // lotesToolStripMenuItem
            // 
            this.lotesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adicionarLoteToolStripMenuItem,
            this.cToolStripMenuItem,
            this.buscarLoteToolStripMenuItem,
            this.listarLoteToolStripMenuItem,
            this.actualizarLoteToolStripMenuItem,
            this.buscarPorParametroLoteToolStripMenuItem,
            this.listarPorParametroLoteToolStripMenuItem});
            this.lotesToolStripMenuItem.Name = "lotesToolStripMenuItem";
            this.lotesToolStripMenuItem.Size = new System.Drawing.Size(58, 24);
            this.lotesToolStripMenuItem.Text = "Lotes";
            // 
            // adicionarLoteToolStripMenuItem
            // 
            this.adicionarLoteToolStripMenuItem.Name = "adicionarLoteToolStripMenuItem";
            this.adicionarLoteToolStripMenuItem.Size = new System.Drawing.Size(267, 26);
            this.adicionarLoteToolStripMenuItem.Text = "Crear Lote";
            this.adicionarLoteToolStripMenuItem.Click += new System.EventHandler(this.adicionarLoteToolStripMenuItem_Click);
            // 
            // cToolStripMenuItem
            // 
            this.cToolStripMenuItem.Name = "cToolStripMenuItem";
            this.cToolStripMenuItem.Size = new System.Drawing.Size(267, 26);
            this.cToolStripMenuItem.Text = "Eliminar Lote";
            this.cToolStripMenuItem.Click += new System.EventHandler(this.cToolStripMenuItem_Click);
            // 
            // buscarLoteToolStripMenuItem
            // 
            this.buscarLoteToolStripMenuItem.Name = "buscarLoteToolStripMenuItem";
            this.buscarLoteToolStripMenuItem.Size = new System.Drawing.Size(267, 26);
            this.buscarLoteToolStripMenuItem.Text = "Buscar Lote";
            this.buscarLoteToolStripMenuItem.Click += new System.EventHandler(this.buscarLoteToolStripMenuItem_Click);
            // 
            // listarLoteToolStripMenuItem
            // 
            this.listarLoteToolStripMenuItem.Name = "listarLoteToolStripMenuItem";
            this.listarLoteToolStripMenuItem.Size = new System.Drawing.Size(267, 26);
            this.listarLoteToolStripMenuItem.Text = "Listar Lote";
            this.listarLoteToolStripMenuItem.Click += new System.EventHandler(this.listarLoteToolStripMenuItem_Click);
            // 
            // actualizarLoteToolStripMenuItem
            // 
            this.actualizarLoteToolStripMenuItem.Name = "actualizarLoteToolStripMenuItem";
            this.actualizarLoteToolStripMenuItem.Size = new System.Drawing.Size(267, 26);
            this.actualizarLoteToolStripMenuItem.Text = "Actualizar Lote";
            this.actualizarLoteToolStripMenuItem.Click += new System.EventHandler(this.actualizarLoteToolStripMenuItem_Click);
            // 
            // buscarPorParametroLoteToolStripMenuItem
            // 
            this.buscarPorParametroLoteToolStripMenuItem.Name = "buscarPorParametroLoteToolStripMenuItem";
            this.buscarPorParametroLoteToolStripMenuItem.Size = new System.Drawing.Size(267, 26);
            this.buscarPorParametroLoteToolStripMenuItem.Text = "Buscar Por parametro Lote";
            this.buscarPorParametroLoteToolStripMenuItem.Click += new System.EventHandler(this.buscarPorParametroLoteToolStripMenuItem_Click);
            // 
            // listarPorParametroLoteToolStripMenuItem
            // 
            this.listarPorParametroLoteToolStripMenuItem.Name = "listarPorParametroLoteToolStripMenuItem";
            this.listarPorParametroLoteToolStripMenuItem.Size = new System.Drawing.Size(267, 26);
            this.listarPorParametroLoteToolStripMenuItem.Text = "Listar por parametro Lote";
            this.listarPorParametroLoteToolStripMenuItem.Click += new System.EventHandler(this.listarPorParametroLoteToolStripMenuItem_Click);
            // 
            // informaciónToolStripMenuItem
            // 
            this.informaciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.acercaDeToolStripMenuItem});
            this.informaciónToolStripMenuItem.Name = "informaciónToolStripMenuItem";
            this.informaciónToolStripMenuItem.Size = new System.Drawing.Size(103, 24);
            this.informaciónToolStripMenuItem.Text = "Información";
            this.informaciónToolStripMenuItem.Click += new System.EventHandler(this.informaciónToolStripMenuItem_Click);
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.acercaDeToolStripMenuItem.Text = "Acerca De:";
            this.acercaDeToolStripMenuItem.Click += new System.EventHandler(this.acercaDeToolStripMenuItem_Click);
            // 
            // GUIPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GUIPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lotesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearProductoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarProductoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarProductoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listarProductosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarPorParametroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listarPorParametroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adicionarLoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarLoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listarLoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarLoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarPorParametroLoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listarPorParametroLoteToolStripMenuItem;
    }
}

